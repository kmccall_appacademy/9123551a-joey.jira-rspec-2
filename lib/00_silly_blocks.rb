def reverser
  string = yield

  string.gsub(/\w+/) { |word| word.reverse }
end

def adder(default = 1)
  yield + default
end

def repeater(default = 1)
  default.times do
    yield
  end
end
